package br.com.mastertech.consumer;

import br.com.mastertech.producer.Livro;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec4-ana-cristina-1", groupId = "anabsouza")
    public void receber(@Payload Livro livro) {
        System.out.println("Recebi o livro " + livro.getName() + " que foi escrito por " + livro.getAuthor());
    }

}
